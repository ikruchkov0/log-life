
const fs = require('fs');
const path = require('path');

if (process.argv.length < 7) {
    console.info('Usage node generate-version.js  <env> <major> <minor> <app path path> <app prefix>');
    return;
}

function listDir(dirPath, dirName) {
    var result = [];
    var subDirs = [];
    fs.readdirSync(dirPath).forEach(file => {
        var localPath = path.join(dirPath, file);
        var localName = path.join(dirName, file);
        var stats = fs.lstatSync(localPath);
        if (stats.isDirectory()) {
            subDirs.push({ path: localPath, name: localName });
        } else if (stats.isFile()) {
            if (file.endsWith('.js')) {
                result.push(localName);
            }
        }
    });  
    return subDirs.reduce((r, dirData) => r.concat(listDir(dirData.path, dirData.name)), result);
}

function getPatch() {
    return Math.floor(new Date() / 1000);
}

var version = {
    env: process.argv[2],
    major: process.argv[3],
    minor: process.argv[4],
    patch: getPatch(),
    content: listDir(process.argv[5], process.argv[6]),
}

console.info(JSON.stringify(version));
