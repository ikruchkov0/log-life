all: front
.PHONY: all

front: service-worker app static front-version
.PHONY: front

node-packages:
	yarn install
.PHONY: all

service-worker: node-packages
	yarn build:service-worker
.PHONY: service-worker

app: node-packages
	yarn build:app
.PHONY: app

static:
	cp -v src/index.html dist/
	cp -rv src/svg dist/
	cp -rv src/icons dist/
	cp -rv src/manifest.json dist/
.PHONY: static

front-version:
	node cmd/generate-version.js local 0 1 dist/js js > dist/version.json
.PHONY: front-version

clean:
	rm -rf dist
.PHONY: clean