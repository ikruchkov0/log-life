import { UpdaterMessage } from '../shared-types';
import { OfflineCache } from './sw-lib/cache';
import { Client } from './sw-lib/client';
import { fallback } from './sw-lib/fallback';
import { fetchWrapper } from './sw-lib/fetch-wrapper';
import { Logger } from './sw-lib/logger';
import { Updater } from './sw-lib/updater';

export class ServiceWorker {
  private client: Client;
  private logger: Logger;
  private updater: Updater;
  private cache: OfflineCache;
  constructor(private scope: ServiceWorkerGlobalScope) {
    this.logger = new Logger();
    this.client = new Client(this.logger);
    this.cache = new OfflineCache(this.scope, this.client, this.logger);
    this.updater = new Updater(this.client, this.logger, this.cache);
  }

  public onInstall(event: Event) {
    this.logger.info('Installed');
  }

  public onActivate(event: ExtendableEvent) {
    event.waitUntil(this.scope.clients.claim());
    event.waitUntil(this.updater.activate());
    this.logger.info('Activated');
  }

  public onFetch(event: FetchEvent) {
    event.respondWith(this.fromCacheOrNetwork(event));
    if (!this.updater.isUpdated(event.request)) {
      event.waitUntil(this.cache.updateCache(event.request));
    }
  }

  public onMessage(event: ExtendableMessageEvent) {
    if (!event.data.type) {
      return;
    }
    const msg = event.data as UpdaterMessage;
    switch (msg.type) {
      case 'start-update':
        this.updater.update();
        break;
      case 'check-updates':
        this.updater.checkUpdates();
        break;
      default:
        this.logger.error('Unknown message ', msg);
    }
  }

  private async fromCacheOrNetwork(event: FetchEvent) {
    try {
      const cachedValue = await this.cache.fromCache(event.request);
      return cachedValue;
    } catch (err) {
      this.logger.error(
        `Unable to get ${event.request.url}
         from cache. Fallback to fetch. Error `,
        err,
      );
      return this.fetchResource(event);
    }
  }

  private async fetchResource(event: FetchEvent) {
    try {
      const response = await fetchWrapper(
        event.request,
        this.client,
        this.logger,
      );
      if (response.ok) {
        return response;
      }
      this.logger.error(
        `Unable to fetch ${
          event.request.url
        }. Fallback to default page. Response is not ok `,
        response,
      );
      return fallback(event.request);
    } catch (err) {
      this.logger.error(
        `Unable to fetch ${
          event.request.url
        }. Fallback to default page. Error `,
        err,
      );
      return fallback(event.request);
    }
  }
}
