import { Client } from './client';
import { Logger } from './logger';

export async function fetchWrapper(request: Request, client: Client, logger: Logger) {
    try {
        let req: Request;
        if (request.mode === 'navigate') {
            req = request;
        } else {
            req = new Request(request, { credentials: 'include', mode: 'same-origin' });
        }
        const response = await fetch(req);
        if (response.ok) {
            await client.tellClientGoOnline();
        } else {
            await client.tellClientGoOffline();
        }
        return response;
    } catch (err) {
        logger.error('Fetch error', err);
        await client.tellClientGoOffline();
        throw err;
    }
}
