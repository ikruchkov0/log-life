const fallbackHTML =
  '<div>\n' +
  '    <div>App Title</div>\n' +
  '    <div>you are offline</div>\n' +
  '</div>';

const fallbackJSON = JSON.stringify({ status: 'NOT FOUND' });

export function fallback(request: Request) {
  if (request.referrer === '' || request.url.endsWith('.html')) {
    return Promise.resolve(
      new Response(fallbackHTML, {
        headers: {
          'Content-Type': 'text/html; charset=utf-8',
        },
        status: 200,
      }),
    );
  }
  return Promise.resolve(
    new Response(fallbackJSON, {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      status: 404,
      statusText: 'Not found',
    }),
  );
}
