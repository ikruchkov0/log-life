export class Logger {
    public info(message?: any, ...optionalParams: any[]) {
        // tslint:disable-next-line:no-console
        console.info(message, ...optionalParams);
    }

    public error(message?: any, ...optionalParams: any[]) {
        // tslint:disable-next-line:no-console
        console.error(message, ...optionalParams);
    }
}
