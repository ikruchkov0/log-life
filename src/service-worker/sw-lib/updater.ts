import { OfflineCache } from './cache';
import { Client } from './client';
import { Logger } from './logger';

const versionUrl = '/version.json';

interface IVersion {
  readonly major: number;
  readonly minor: number;
  readonly patch: number;
  readonly env: 'development' | 'production' | 'none';
  readonly content: string[];
}

interface IVersionData {
  data: IVersion | null;
  response: Response;
}

const defaultVersion: IVersion = {
  content: [],
  env: 'none',
  major: -1,
  minor: -1,
  patch: -1,
};

export class Updater {
  private currentVersion: IVersion = defaultVersion;
  private updateInProgress = false;

  constructor(
    private client: Client,
    private logger: Logger,
    private cache: OfflineCache,
  ) {}

  public async activate() {
    const oldVersion = await this.getOldVersion();
    if (oldVersion && oldVersion.env !== 'development') {
      this.currentVersion = oldVersion;
    }
  }

  public async checkUpdates() {
    const currentVersionData = await this.getCurrentVersion();
    if (!currentVersionData) {
      this.client.tellClientNoUpdates();
      return;
    }

    if (this.isSameVersion(this.currentVersion, currentVersionData.data)) {
      this.client.tellClientNoUpdates();
      return;
    }

    this.client.tellClientHasUpdates(currentVersionData.data.content.length);
  }

  public async update() {
    if (this.updateInProgress) {
      return;
    }
    const currentVersionData = await this.getCurrentVersion();
    if (!currentVersionData) {
      return;
    }

    if (this.isSameVersion(this.currentVersion, currentVersionData.data)) {
      return;
    }

    const total = currentVersionData.data.content.length;
    let processed = 0;
    const currentVersion = currentVersionData.data;

    this.updateInProgress = true;
    this.client.tellClientUpdateStarted(total);

    currentVersion.content.map(async (p) =>
      this.cache.updateCache(new Request(p)).then(() => {
        processed++;
        this.client.tellClientUpdateProgress(total, processed);
        if (processed === total) {
          return this.saveCurrentVersion(currentVersionData);
        }
      }),
    );
  }

  public isUpdated(request: Request) {
    if (this.currentVersion.env === 'development') {
      return false;
    }
    const item = this.currentVersion.content.find((s) => request.url.endsWith(s));
    return !!item;
  }

  private async saveCurrentVersion(versionData: IVersionData) {
    if (!versionData.data) {
      return;
    }
    const cache = await this.cache.getCache();
    await cache.put(versionUrl, versionData.response);
    this.currentVersion = versionData.data;
  }

  private async getOldVersion() {
    const cache = await this.cache.getCache();
    const oldVersionResponse = await cache.match(versionUrl);
    if (!oldVersionResponse || !oldVersionResponse.ok) {
      this.logger.error(
        'Unable to load old version. Response: ',
        oldVersionResponse,
      );
      return null;
    }
    return (await oldVersionResponse.json()) as IVersion;
  }

  private async getCurrentVersion() {
    const response = await fetch(versionUrl);
    const responseClone = response.clone();
    if (!response || !response.ok) {
      this.logger.error('Unable to load version. Response: ', response);
      return null;
    }

    const data = (await response.json()) as IVersion;
    return {
      data,
      response: responseClone,
    };
  }

  private isSameVersion(old: IVersion | null, current: IVersion | null) {
    return (
      old &&
      current &&
      current.env !== 'development' &&
      old.major === current.major &&
      old.minor === current.minor &&
      old.patch === current.patch
    );
  }
}
