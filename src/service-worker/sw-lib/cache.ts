import { Client } from './client';
import { fetchWrapper } from './fetch-wrapper';
import { Logger } from './logger';

const CACHE = 'offline-fallback-v1';

export class OfflineCache {
  constructor(
    private scope: ServiceWorkerGlobalScope,
    private client: Client,
    private logger: Logger,
  ) {}

  public getCache() {
    return this.scope.caches.open(CACHE);
  }

  public async fromCache(request: Request) {
    const cache = await this.getCache();
    const matching = await cache.match(request);
    return matching || Promise.reject('no-match');
  }

  public async updateCache(request: Request) {
    try {
      const response = await fetchWrapper(request, this.client, this.logger);
      if (response.ok) {
        const cache = await this.getCache();
        await cache.put(request, response);
        this.logger.info('Cache updated for ', request.url);
      } else {
        this.logger.error(
          'Unable to update cache for ' + request.url + 'response: ',
          response,
        );
      }
    } catch (err) {
        this.logger.error('Unable to update cache for ' + request.url, err);
    }
    return;
  }
}
