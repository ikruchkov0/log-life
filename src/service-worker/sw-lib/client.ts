import {
  Channels,
  ConnectionStatusMessage,
  UpdaterMessage,
} from '../../shared-types';

import { Logger } from './logger';

export class Client {
  constructor(private logger: Logger) {}

  public tellClientGoOffline() {
    return this.tellClient<ConnectionStatusMessage>('connection_status', {
      type: 'go-offline',
    });
  }

  public tellClientGoOnline() {
    return this.tellClient<ConnectionStatusMessage>('connection_status', {
      type: 'go-online',
    });
  }

  public tellClientUpdateStarted(total: number) {
    return this.tellClient<UpdaterMessage>('updater', {
      total,
      type: 'update-started',
    });
  }

  public tellClientHasUpdates(total: number) {
    return this.tellClient<UpdaterMessage>('updater', {
      total,
      type: 'has-updates',
    });
  }

  public tellClientNoUpdates() {
    return this.tellClient<UpdaterMessage>('updater', {
      type: 'no-updates',
    });
  }

  public tellClientUpdateProgress(total: number, loaded: number) {
    return this.tellClient<UpdaterMessage>('updater', {
      loaded,
      total,
      type: 'progress',
    });
  }

  private tellClient<T>(channelType: Channels, message: T) {
    try {
      const channel = new BroadcastChannel(channelType);
      channel.postMessage(message);
      channel.close();
    } catch (err) {
      this.logger.error('Unable to tell client');
    }
  }
}
