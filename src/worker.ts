import { ServiceWorker } from './service-worker/index';

((global: ServiceWorkerGlobalScope) => {
  const sw = new ServiceWorker(global);

  global.addEventListener('install', (event) => {
    sw.onInstall(event);
  });

  global.addEventListener('activate', (event) => {
    sw.onActivate(event);
  });

  global.addEventListener('fetch', (event) => {
    sw.onFetch(event);
  });
  global.addEventListener('message', (event) => {
    sw.onMessage(event);
  });
})((self as any) as ServiceWorkerGlobalScope);
