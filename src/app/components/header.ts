import { BaseComponent } from './base-component';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
    <span>Header</span>
`;

class Header extends BaseComponent {
    constructor() {
        super(tmpl);
      }
}

export const headerTag = 'log-header';

window.customElements.define(headerTag, Header);