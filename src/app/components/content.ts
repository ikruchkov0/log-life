import { BaseComponent } from './base-component';

const optionalContainerId = 'optional';
const addOptionalId = 'addOptional';
const tmpl = document.createElement('template');
tmpl.innerHTML = `
  <button id="${addOptionalId}">Add optional</button>
  <div id="${optionalContainerId}"></div>
`;

class Content extends BaseComponent {
    constructor() {
        super(tmpl);

        const optionalContainer = this.getChildById(optionalContainerId);

        const addOptionalBtn = this.getChildById(addOptionalId);
        if (!addOptionalBtn) {
            throw new Error(`Unable to find #${addOptionalId}`);
        }

        addOptionalBtn.addEventListener('click', async () => {
            try {
                const module = await import('./optional');
                const optional = document.createElement(module.default);
                optionalContainer.appendChild(optional);
            } catch (err) {
                console.error(err);
            }
        });

    }
}

export const contentTag = 'log-content';

window.customElements.define(contentTag, Content);