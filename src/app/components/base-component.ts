export abstract class BaseComponent extends HTMLElement {
    protected root: ShadowRoot;

    constructor(tmpl: HTMLTemplateElement) {
      super();
  
      this.root = this.attachShadow({ mode: 'open' });
      this.root.appendChild(tmpl.content.cloneNode(true));
    }

    protected getChildById(id: string) {
        const child = this.root.getElementById(id);
        if (!child) {
            throw new Error(`Unable to get child by id ${id}`);
        }
        return child;
    }

  }