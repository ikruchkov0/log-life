import { BaseComponent } from './base-component';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
  <span>Sidebar</span>
`;

class Sidebar extends BaseComponent {
    constructor() {
        super(tmpl);
      }
}

export const sidebarTag = 'log-sidebar';

window.customElements.define(sidebarTag, Sidebar);