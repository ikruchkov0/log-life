import { BaseComponent } from './base-component';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
  <div>Optional1</div>
`;

class Optional extends BaseComponent {
    constructor() {
        super(tmpl);
      }
}

const tag = 'log-optional';

window.customElements.define(tag, Optional);

export default tag;