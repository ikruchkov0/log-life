import { BaseComponent } from './base-component';

import { UpdaterMessage } from 'shared-types';
import { createUpdaterStatusBus, BusSubscriber } from '../bus';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
<style>
    .offline-label{
        color: #e08484;
        text-transform: uppercase;
        text-align: center;
    }
    .hidden {
        display: none !important;
    }
</style>
<span id="progress-label"></span>
`;

const token = 'updater-status-component';

class UpdaterStatus extends BaseComponent {

    private bus: BusSubscriber<UpdaterMessage> | null;
    private progressLabel: HTMLElement;

    constructor() {
        super(tmpl);
        this.bus = null;
        this.progressLabel = this.getChildById('progress-label');
    }

    connectedCallback() {
        this.bus = createUpdaterStatusBus();

        this.bus.subscribe(token, (msg) => {
            switch (msg.type) {
                case 'no-updates':
                    this.progressLabel.innerText = 'No updates';
                break;
                case 'has-updates':
                    this.progressLabel.innerText = `Has updates  ${msg.total}`;
                break;
                case 'update-started':
                    this.progressLabel.innerText = `Update: 0/${msg.total}`;
                break;
                case 'progress':
                    this.progressLabel.innerText = `Update: ${msg.loaded}/${msg.total}`;
                break;
            }
        });
    }

    disconnectedCallback() {
        if (!this.bus) {
            return;
        }
        this.bus.unsubscribe(token);
    }
}

export const updaterStatusTag = 'log-updater-status';

window.customElements.define(updaterStatusTag, UpdaterStatus);