import { BaseComponent } from './base-component';

import { connectionStatusTag } from './connection-status';
import { updaterStatusTag } from './updater-status';
import { UpdaterMessage } from 'shared-types';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
  <${connectionStatusTag}></${connectionStatusTag}>
  <${updaterStatusTag}></${updaterStatusTag}>
  <button id="update">Update</button>
  <button id="check">Check</button>
`;

class Footer extends BaseComponent {
    constructor() {
        super(tmpl);
    
        let updateBtn = this.getChildById('update');
        updateBtn.addEventListener('click', () => {
            let sw = window.navigator.serviceWorker.controller;
            if (!sw) {
              return;
            }
            sw.postMessage({
              type: 'start-update'
            } as UpdaterMessage);
        });

        let checkBtn = this.getChildById('check');

        checkBtn.addEventListener('click', () => {
            let sw = window.navigator.serviceWorker.controller;
            if (!sw) {
              return;
            }
            sw.postMessage({
              type: 'check-updates'
            } as UpdaterMessage);
        });

      }
}

export const footerTag = 'log-footer';

window.customElements.define(footerTag, Footer);