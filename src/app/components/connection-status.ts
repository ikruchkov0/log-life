
import { BaseComponent } from './base-component';
import { ConnectionStatusMessage } from 'shared-types';
import { createConnectionStatusBus, BusSubscriber } from '../bus';


const tmpl = document.createElement('template');
tmpl.innerHTML = `
<style>
    .offline-label{
        color: #e08484;
        text-transform: uppercase;
        text-align: center;
    }
    .hidden {
        display: none !important;
    }
    #connection-icon{
        width: 10px;
        height: 10px;
        background-color: black;
        border-radius: 5px;
        display: inline-block;
    }
    #connection-icon.connected{
        background-color: green;
    }
    #connection-icon.disconnected{
        background-color: red;
    }
</style>
  <i id="connection-icon"></i>
`;

const token = 'connection-status-component';

class ConnectionStatus extends BaseComponent {

    private bus: BusSubscriber<ConnectionStatusMessage> | null;
    private connectionIcon: HTMLElement;

    constructor() {
        super(tmpl);

        this.bus = null;
        this.connectionIcon = this.getChildById('connection-icon');
    }

    connectedCallback() {
        this.bus = createConnectionStatusBus();
        this.bus.subscribe(token, (e) => this.processMessage(e));
    }

    disconnectedCallback() {
        if (!this.bus) {
            return;
        }
        this.bus.unsubscribe(token);
    }

    private processMessage(msg: ConnectionStatusMessage) {
        switch (msg.type) {
            case 'go-online':
                this.connectionIcon.classList.remove('disconnected');
                this.connectionIcon.classList.add('connected');
                break;
            case 'go-offline':
            this.connectionIcon.classList.remove('connected');
                this.connectionIcon.classList.add('disconnected');
                break;
        }
    }
}

export const connectionStatusTag = 'log-connection-status';

window.customElements.define(connectionStatusTag, ConnectionStatus);