import { BaseComponent } from './base-component';

import { headerTag } from './header';
import { footerTag } from './footer';
import { contentTag } from './content';
import { sidebarTag } from './sidebar';

const tmpl = document.createElement('template');
tmpl.innerHTML = `
  <style>
  ${sidebarTag} {
    grid-area: sidebar;
  }
  
  ${contentTag} {
    grid-area: content;
    position: relative;
  }
  
  ${headerTag} {
    grid-area: header;
  }
  
  ${footerTag} {
    grid-area: footer;
  }
  
  .wrapper {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: 150px auto;
    grid-template-areas:
      "header  header "
      "sidebar content"
      "footer  footer ";
    background-color: #fff;
    color: #444;
  }
  
  ${headerTag},
  ${footerTag},
  ${contentTag},
  ${sidebarTag} {
    background-color: #444;
    color: #fff;
    border-radius: 5px;
    padding: 50px;
    font-size: 150%;
  }
  
  ${headerTag},
  ${footerTag} {
    background-color: #999;
    padding: 10px;
  }
  </style>
  <div class="wrapper">
    <${headerTag} box></${headerTag}>
    <${sidebarTag} box></${sidebarTag}>
    <${contentTag} box></${contentTag}>
    <${footerTag} box>Footer</${footerTag}>
  </div>
`;

class App extends BaseComponent {
  constructor() {
    super(tmpl);
  }
}

export const appTag = 'log-app';

window.customElements.define(appTag, App);

