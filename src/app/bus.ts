
import { Channels, ConnectionStatusMessage, UpdaterMessage } from 'shared-types';

interface Listener<T> {
    (msg: T): void;
}

export class BusSubscriber<T> {
    private listener: any;
    private listeners: Map<string, Listener<T>>;
    constructor(private channel: BroadcastChannel) {
        this.listeners = new Map<string, Listener<T>>();
        this.listener = (e: {data: T}) => {
            for(let p of this.listeners) {
                p[1](e.data);
            }
        };
        this.channel.addEventListener('message', this.listener);
    }

    subscribe(token: string, listener: Listener<T>) {
        this.listeners.set(token, listener);
    }

    unsubscribe(token: string) {
        this.listeners.delete(token);
    }

    /*close() {
        if (!this.channel) {
            return;
        }
        this.listeners.clear();
        this.channel.removeEventListener('message', this.listener);
        this.channel.close();
    }*/
}

let connectionStatusBus: BusSubscriber<ConnectionStatusMessage>;
let updaterStatusBus: BusSubscriber<UpdaterMessage>;

export function createConnectionStatusBus() {
    if (!connectionStatusBus) {
        connectionStatusBus = new BusSubscriber<ConnectionStatusMessage>(createChannel('connection_status'));
    }
    return connectionStatusBus;
}

export function createUpdaterStatusBus() {
    if (!updaterStatusBus) {
        updaterStatusBus = new BusSubscriber<UpdaterMessage>(createChannel('updater'));
    }
    return updaterStatusBus;
}

function createChannel(channelType: Channels) {
    return new BroadcastChannel(channelType);
}