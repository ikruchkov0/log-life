(() => {
    document.addEventListener('DOMContentLoaded', () => init());

    async function init() {
        const spinner = document.getElementById('spinner');
        if (!spinner) {
            throw new Error(`Unable to find spinner`);
        }
        const {appTag} = await import('./app/components/app');
       
        const app = document.createElement(appTag);
        
        document.body.appendChild(app);
        await startWorker();
        spinner.remove(); 
        await startWorker();
    }

    async function startWorker() {
        if ('serviceWorker' in navigator) {
            try {
                let worker = await navigator.serviceWorker.register('./worker.ts');
                worker.sync.register('log-life-update');
            } catch (err) {
                console.error(err);
            }
        }
    }
})();

