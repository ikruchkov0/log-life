export interface ConnectionStatusMessage {
    type: 'go-online' | 'go-offline';
}
export interface UpdaterMessage {
    type: 'start-update' | 'update-started' | 'progress' | 'no-updates' | 'has-updates' | 'check-updates';
    total?: number;
    loaded?: number;
}
export type Channels = 'connection_status' | 'updater';